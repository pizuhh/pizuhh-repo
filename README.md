# pizza-repo
Pacman repo (experimental)
Mainly for personal use

# How do add?
1. make backup of your pacman.conf ``sudo cp /etc/pacman.conf /etc/pacman.conf.back``
2. add these line at the bottom of ``pacman.conf`` (make sure you run your texteditor with root privileges) 
```bash
[pizza-repo]
SigLevel = Optional TrustedOnly
Server = https://raw.githubusercontent.com/pizzuhh/pizza-repo/main/x86_64/
```
